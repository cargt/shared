/*
 * iop_parser.c
 *
 *  Created by Peter Carlson on Oct 2, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 */


#include "string.h"

#include "CARGT.h"
#include "IOP.h"

#ifndef IOP_RAM_LITE
	static uint8_t        raw_rx_data[IOP_BUF_SIZE* 2];
	static uint8_t        unstuffed_buf[IOP_BUF_SIZE*2];
#endif

//Function Protoypes
static void copy_byte
    (
    IOP_ctrl_type *			ctrl,
    uint8_t 				data_byte
    );

#ifdef IOP_RAM_LITE
static void insert_byte
    (
    IOP_ctrl_type *			ctrl,
    uint8_t 				data_byte,
	bool					skip_dle
    );
#endif

static void reset_state_machine
    (
    IOP_ctrl_type *			ctrl
    );

#ifdef IOP_RAM_LITE
static void reset_tx_state_machine
    (
    IOP_ctrl_type *			ctrl
    );
#endif

static bool validate_rx_message
    (
    IOP_msg_type *	iop_msg
    );

/*
*   PROCEDURE NAME:
*       IOP_init
*
*   DESCRIPTION:
*
*/
void IOP_init
    ( void )
{
IOP_msg_hndlr_init();
}	/* IOP_init() */


/*
*   PROCEDURE NAME:
*       IOP_parse_rx_data
*
*   DESCRIPTION:
*
*/
#ifdef IOP_RAM_LITE
bool IOP_parse_rx_data
    (
    IOP_ctrl_type *			ctrl,
	uint8_t const *         rx_data,
	uint32_t                size
	)
{
//Local Variables
int  				idx;
uint8_t const *		data;
uint32_t            bytes_read;
bool				ret = false;

bytes_read = size;
data = rx_data;

for( idx = 0; idx < bytes_read; idx++ )
	{
	switch( ctrl->rx_state )
		{
		//Look for beginning of a new message
		case IOP_GET_HEADER_SYNC:

			if( data[idx] == IOP_DLE_BYTE )
				{
				copy_byte( ctrl, data[idx] );
				ctrl->rx_state = IOP_GET_HEADER_ACK;

				ret = true;
				}
			//Else stay in this state and keep looking for
			//the beginning of a message
			else
				{
				//ERROR: BYTE IGNORED WHILE LOOKING FOR HEADER SYNC
				ret = false;
				}
			break;

		case IOP_GET_HEADER_ACK:
			if( data[idx] < IOP_MSG_DESCRIPTOR_INVLD )
				{
				copy_byte( ctrl, data[idx] );
				ctrl->rx_state = IOP_GET_DATA;

				ret = true;
				}
			else
				{
				reset_state_machine( ctrl );

				ret = false;
				}
			break;

		case IOP_GET_DATA:
			//Either beginning of message tail or escapted data byte
			if( data[idx] == IOP_DLE_BYTE )
				{
				ctrl->rx_state = IOP_GET_DATA_DLE;
				}
			copy_byte( ctrl, data[idx] );

			ret = true;
			break;

		case IOP_GET_DATA_DLE:
			//Escaped data byte
			if( data[idx] == IOP_DLE_BYTE )
				{
				ctrl->rx_state = IOP_GET_DATA;
				ctrl->iop_rx_msg->num_esc_bytes++;

				ret = true;
				}

			//End of message detected
			else if( data[idx] == IOP_ETX_BYTE )
				{
				copy_byte( ctrl, data[idx] );

				//Validate message then send for processing
				if( validate_rx_message( ctrl->iop_rx_msg ) )
					{
					IOP_rx_msg_hndlr( ctrl, ctrl->iop_rx_msg->header.id, (void*)ctrl->iop_rx_msg, ctrl->iop_rx_msg->header.data_length );
					}

				//Reset state machine and read in next message
				reset_state_machine( ctrl );

				ret = true;
				}

			//Should never get here; restart state machine
			else
				{
				reset_state_machine( ctrl );

				ret = false;
				}
			break;

		default:
			/* Do nothing */
			ret = false;
			break;
		}
	}

return ret;
}

#else

bool IOP_parse_rx_data
    (
    IOP_ctrl_type *				ctrl,
    const uint32_t        		num_bytes
    )
{
//Local Variables
int  				idx;
uint8_t * 			data;
uint32_t            bytes_read;
bool				ret = false;

//Read data from circular buffer
bytes_read = UTL_circ_buf_read( ctrl->rx_cb, &raw_rx_data[0], num_bytes );
//assert( bytes_read == num_bytes );
data = &raw_rx_data[0];

//Feed each byte through the state machine
for( idx = 0; idx < bytes_read; idx++ )
	{
	switch( ctrl->rx_state )
		{
		//Look for beginning of a new message
		case IOP_GET_HEADER_SYNC:

			if( data[idx] == IOP_DLE_BYTE )
				{
				copy_byte( ctrl, data[idx] );
				ctrl->rx_state = IOP_GET_HEADER_ACK;

				ret = true;
				}
			//Else stay in this state and keep looking for
			//the beginning of a message
			else
				{
				//ERROR: BYTE IGNORED WHILE LOOKING FOR HEADER SYNC
				ret = false;
				}
			break;

		case IOP_GET_HEADER_ACK:
			if( data[idx] < IOP_MSG_DESCRIPTOR_INVLD )
				{
				copy_byte( ctrl, data[idx] );
				ctrl->rx_state = IOP_GET_DATA;

				ret = true;
				}
			else
				{
				reset_state_machine( ctrl );

				ret = false;
				}
			break;

		case IOP_GET_DATA:
			//Either beginning of message tail or escapted data byte
			if( data[idx] == IOP_DLE_BYTE )
				{
				ctrl->rx_state = IOP_GET_DATA_DLE;
				}
			copy_byte( ctrl, data[idx] );

			ret = true;
			break;

		case IOP_GET_DATA_DLE:
			//Escaped data byte
			if( data[idx] == IOP_DLE_BYTE )
				{
				ctrl->rx_state = IOP_GET_DATA;
				ctrl->iop_rx_msg->num_esc_bytes++;

				ret = true;
				}

			//End of message detected
			else if( data[idx] == IOP_ETX_BYTE )
				{
				copy_byte( ctrl, data[idx] );

				//Validate message then send for processing
				if( validate_rx_message( ctrl->iop_rx_msg ) )
					{
					IOP_rx_msg_hndlr( ctrl, ctrl->iop_rx_msg->header.id, (void*)ctrl->iop_rx_msg, ctrl->iop_rx_msg->header.data_length );
					}

				//Reset state machine and read in next message
				reset_state_machine( ctrl );

				ret = true;
				}
			//Should never get here; restart state machine
			else
				{
				reset_state_machine( ctrl );

				ret = false;
				}
			break;

		default:
			/* Do nothing */
			ret = false;
			break;
		}
	}

return ret;
}   /* IOP_parse_rx_data() */

#endif

/*
*   PROCEDURE NAME:
*       IOP_pwrp
*
*   DESCRIPTION:
*
*/
void IOP_pwrp
    ( void )
{
IOP_msg_hndlr_pwrp();
}	/* IOP_pwrp() */


/*
*   PROCEDURE NAME:
*       IOP_register
*
*   DESCRIPTION:
*
*/
void IOP_register
	(
	IOP_ctrl_type *			ctrl,
	uint32_t				tx_buf_size,
	uint32_t				rx_buf_size
	)
{
#ifndef IOP_RAM_LITE
	UTL_circ_buf_init( ctrl->tx_cb, ctrl->tx_buf, tx_buf_size );
	UTL_circ_buf_init( ctrl->rx_cb, ctrl->rx_buf, rx_buf_size );
#endif

//Only initialize IOP buffers and control structures once
if( !(ctrl->inited) )
	{
	ctrl->inited = true;
	reset_state_machine( ctrl );
#ifdef IOP_RAM_LITE
	reset_tx_state_machine( ctrl );
#endif
	}

}	/* IOP_register() */


/*
*   PROCEDURE NAME:
*       IOP_tx_msg
*
*   DESCRIPTION:
*       [procedure description]
*
*/
void IOP_tx_msg
    (
    IOP_ctrl_type *				ctrl,
    iop_inst_id_type    		id,
    iop_msg_descriptor_type		dscptr,
    uint8_t const *             data,
    uint32_t                    size
    )
{
//Local Variables
uint32_t                      i;

#ifdef IOP_RAM_LITE
	ctrl->tx_ctrl.payload_length = size;

	//Create message header
	insert_byte( ctrl, IOP_DLE_BYTE, true );
	insert_byte( ctrl, dscptr, false );
	insert_byte( ctrl, (id & 0xFF00) >> 8, false );
	insert_byte( ctrl, id & 0x00FF, false );
	insert_byte( ctrl, (size & 0xFF000000) >> 24, false );
	insert_byte( ctrl, (size & 0x00FF0000) >> 16, false );
	insert_byte( ctrl, (size & 0x0000FF00) >> 8, false );
	insert_byte( ctrl, size & 0x000000FF, false );

	//Copy message payload
	for( i = 0; i < size; i++ )
	{
		insert_byte( ctrl, data[i], false );
	}

	//Populate message tail
    uint16_t chksm = ctrl->tx_ctrl.chksm;
    insert_byte( ctrl, (chksm & 0xFF00 )>> 8, false );
    insert_byte( ctrl, (chksm & 0x00FF), false );
	insert_byte( ctrl, IOP_DLE_BYTE, true );
	insert_byte( ctrl, IOP_ETX_BYTE, true );

	//Transmit message to receiver
	if( ctrl->tx_func != NULL )
		{
		ctrl->tx_func( ctrl->tx_buf, ctrl->tx_ctrl.buf_idx );
		}

	reset_tx_state_machine( ctrl );
#else

	uint16_t                      chksm;
	uint32_t                      num_esc_bytes;
	uint8_t                       dle_val;
	uint32_t                      mem_size;

	//Build IOP formatted message
	//Populate message header
	unstuffed_buf[0] = IOP_DLE_BYTE;
	unstuffed_buf[1] = dscptr;
	unstuffed_buf[2] = (uint8_t)( (id & 0xFF00) >> 8 );
	unstuffed_buf[3] = (uint8_t)( id & 0x00FF );
	unstuffed_buf[4] = (uint8_t)( (size & 0xFF000000) >> 24);
	unstuffed_buf[5] = (uint8_t)( (size & 0x00FF0000) >> 16);
	unstuffed_buf[6] = (uint8_t)( (size & 0x0000FF00) >> 8);
	unstuffed_buf[7] = (uint8_t)( size & 0x000000FF );
	if( size && data != NULL )
		{
		memcpy( &unstuffed_buf[8], data, size );
		}

	//Calculate checksum and number of bytes that need to be escaped
	chksm = 0;
	num_esc_bytes = 0;
	for( i = 0; i < 8+size; i++ )
		{
		chksm += unstuffed_buf[i];
		if( ( unstuffed_buf[i] == IOP_DLE_BYTE ) && ( i != 0 ) )
			{
			num_esc_bytes++;
			}
		}

	//Populate message tail
	unstuffed_buf[8+size] = (uint8_t)( (chksm & 0xFF00 )>> 8);
	unstuffed_buf[9+size] = (uint8_t)(chksm & 0x00FF);
	unstuffed_buf[10+size] = IOP_DLE_BYTE;
	unstuffed_buf[11+size] = IOP_ETX_BYTE;

	//Calculate number of tail bytes that need to be escaped
	for( i = 8+size; i < 10+size; i++ )
		{
		if( unstuffed_buf[i] == IOP_DLE_BYTE )
			{
			num_esc_bytes++;
			}
		}

	//Determine total message size
	mem_size = 12 + size + num_esc_bytes;

	//Copy message into dynamic buffer while stuffing
	for( i = 0; ( i < ( 12 + size ) ); i++ )
		{
		UTL_circ_buf_write( ctrl->tx_cb, &unstuffed_buf[i], 1 );
		if( (unstuffed_buf[i] == IOP_DLE_BYTE) && (i != 0) && (i != 10+size) )
			{
			//Stuff BLE byte
			dle_val = IOP_DLE_BYTE;
	        UTL_circ_buf_write( ctrl->tx_cb, &dle_val, 1 );
			}
		}

	//Transmit message to receiver
	if( ctrl->tx_func != NULL )
		{
		ctrl->tx_func( ctrl->tx_cb, mem_size );
		}
#endif

}   /* IOP_tx_msg() */


/*
*   PROCEDURE NAME:
*       copy_byte
*
*   DESCRIPTION:
*
*/
static void copy_byte
    (
    IOP_ctrl_type *			ctrl,
    uint8_t 				data_byte
    )
{

if( ctrl->iop_rx_msg->total_length < IOP_BUF_SIZE )
    {
	ctrl->iop_rx_msg->data[ctrl->iop_rx_msg->total_length++ ] = data_byte;
	ctrl->iop_rx_msg->calc_chksm += data_byte;
    }
else
    {
    //ERROR: DATA BUFFER NEARLY OVERFLOWED!! ABANDON MESSAGE
    reset_state_machine( ctrl );
    }
}   /* copy_byte() */


/*
*   PROCEDURE NAME:
*       insert_byte
*
*   DESCRIPTION:
*
*/
#ifdef IOP_RAM_LITE
static void insert_byte
    (
    IOP_ctrl_type *			ctrl,
    uint8_t 				data_byte,
	bool					skip_dle
    )
{

if( ctrl->tx_ctrl.buf_idx < ctrl->tx_buf_length )
	{
	//Check to see if DLE byte needs to be inserted
	if( data_byte == IOP_DLE_BYTE && ctrl->tx_ctrl.buf_idx != 0 && !skip_dle )
		{
		ctrl->tx_buf[ctrl->tx_ctrl.buf_idx++] = IOP_DLE_BYTE;
		}

	 ctrl->tx_ctrl.chksm += data_byte;
	 ctrl->tx_buf[ctrl->tx_ctrl.buf_idx++] = data_byte;
	}
else
    {
    //ERROR: TX DATA BUFFER NEARLY OVERFLOWED!! ABANDON MESSAGE
	reset_tx_state_machine( ctrl );
    }
}	/* insert_byte() */
#endif

/*
*   PROCEDURE NAME:
*       reset_state_machine
*
*   DESCRIPTION:
*
*/
static void reset_state_machine
    (
    IOP_ctrl_type *			ctrl
    )
{

ctrl->rx_state = IOP_GET_HEADER_SYNC;

memset( ctrl->iop_rx_msg, 0xFF, sizeof( *(ctrl->iop_rx_msg) ) );
ctrl->iop_rx_msg->total_length    = 0;
ctrl->iop_rx_msg->calc_chksm      = 0;
ctrl->iop_rx_msg->num_esc_bytes   = 0;

if( ctrl->reset_func != NULL )
	{
	ctrl->reset_func();
	}

}   /* reset_state_machine() */


#ifdef IOP_RAM_LITE
static void reset_tx_state_machine
    (
    IOP_ctrl_type *			ctrl
    )
{
ctrl->tx_ctrl.buf_idx = 0;
ctrl->tx_ctrl.num_esc_bytes = 0;
ctrl->tx_ctrl.payload_length = 0;
ctrl->tx_ctrl.chksm = 0;
}
#endif

/*
*   PROCEDURE NAME:
*       validate_rx_message
*
*   DESCRIPTION:
*
*/
static bool validate_rx_message
    (
    IOP_msg_type *	iop_msg
    )
{
#define MIN_IOP_MSG_BYTES ( sizeof( iop_hdr_type ) + sizeof( iop_tail_type ) )

//Validate minimum message size
if( iop_msg->total_length < MIN_IOP_MSG_BYTES )
    {
    return false;
    }

//Parse message header
iop_msg->header.header_dle = iop_msg->data[0];
iop_msg->header.descriptor = iop_msg->data[1];
iop_msg->header.id = ( iop_msg->data[2] << 8 ) | ( iop_msg->data[3] ); //ID transmitted MSB first
iop_msg->header.data_length = ( iop_msg->data[4] << 24 ) | ( iop_msg->data[5] << 16 ) | ( iop_msg->data[6] << 8 ) | ( iop_msg->data[7] ); //Data length transmitted MSB first

//Verify data length
if( iop_msg->total_length != ( MIN_IOP_MSG_BYTES + iop_msg->header.data_length ) )
    {
    return false;
    }

//Verify checksum
//- Adjust our calculated checksum to not account for bytes
//  in message tail
iop_msg->calc_chksm    -= ( iop_msg->data[iop_msg->total_length - 4] + iop_msg->data[iop_msg->total_length - 3] +
							iop_msg->data[iop_msg->total_length - 2] + iop_msg->data[iop_msg->total_length - 1] );

memcpy( &iop_msg->tail, &iop_msg->data[iop_msg->total_length - 4], sizeof( iop_tail_type ) );
iop_msg->tail.chksm = ( iop_msg->data[iop_msg->total_length - 4] << 8 ) | iop_msg->data[iop_msg->total_length - 3]; //Chksm transmitted MSB

if( iop_msg->tail.chksm != iop_msg->calc_chksm )
    {
    return false;
    }

return true;
}   /* validate_rx_message() */
