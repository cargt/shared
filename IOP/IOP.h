/*
 * IOP.h
 *
 *  Created by Peter Carlson on Oct 2, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 */

#ifndef IOP_H_
#define IOP_H_

#include "CARGT.h"

#ifndef IOP_RAM_LITE
    #include "UTL_circ_buf.h"
#endif

#define IOP_DLE_BYTE                    ( 0x10 )
#define IOP_ETX_BYTE                    ( 0x03 )

#ifndef IOP_BUF_SIZE
	#define IOP_BUF_SIZE		( 64 )
#endif

//IOP TX function pointer
#ifdef IOP_RAM_LITE
	typedef void (* IOP_tx_func_type)( uint8_t const * buf, uint32_t size );
#else
	typedef void (* IOP_tx_func_type)( UTL_circ_buf_t * circ_buf, uint32_t size );
#endif

//IOP RX message handler function pointer
typedef void (* IOP_rx_msg_func_type)( void * ctrl, uint16_t id, uint8_t const * data, uint32_t size );

//MISC TYPE DEFINITIONS
typedef uint8_t iop_msg_descriptor_type; enum    /* AI2 Header Message Descriptor Field */
    {
    IOP_MSG_DESCRIPTOR_NO_ACK_REQ = 0x00,      /* No ACK requested                 */
    IOP_MSG_DESCRIPTOR_ACK_REQ    = 0x01,      /* ACK requested                    */
    IOP_MSG_DESCRIPTOR_ACK_RESP   = 0x02,      /* ACK response                     */
    IOP_MSG_DESCRIPTOR_INVLD      = 0x03,      /* Invalid descriptor               */

    IOP_MSG_DESCRIPTOR_NMBR                    /* Number of Message Descriptors    */
    };

typedef uint8_t IOP_rx_state_type; enum
    {
    IOP_GET_HEADER_SYNC = 0,        /* Header Start Byte                */
    IOP_GET_HEADER_ACK,             /* Header ACK Byte                  */
    IOP_GET_DATA,                   /* Generic Data Byte                */
    IOP_GET_DATA_DLE,               /* Check for escaped DLE byte       */
    IOP_STATE_NMBR                  /* Number of States                 */
    };

//IOP Command Message IDs
typedef uint16_t iop_cmnd_msg_type; enum
    {
	IOP_CMND_MSG_START	= 0,

	IOP_CMND_MSG_LAST,			//Must be last
	IOP_CMDN_MSG_CNT = IOP_CMND_MSG_LAST
    };

//IOP Event IDs
typedef uint16_t iop_evnt_id_type; enum
    {
	IOP_EVNT_ID_START	= 0,

	IOP_EVNT_ID_LAST,			//Must be last
	IOP_EVNT_ID_CNT 	= IOP_EVNT_ID_LAST
    };

#pragma pack(1)
typedef struct
    {
    uint8_t                     header_dle;
    uint8_t     				descriptor;
    uint16_t        			id;
    uint32_t                    data_length;
    } iop_hdr_type;
#pragma pack()

#pragma pack(1)
typedef struct
    {
    uint16_t                    chksm;
    uint8_t                     tail_dle;
    uint8_t                     tail_etx;
    } iop_tail_type;
#pragma pack()

typedef struct
    {
    iop_hdr_type         		header;
    uint8_t                     data[IOP_BUF_SIZE];
    iop_tail_type        		tail;
    uint16_t                    calc_chksm;
    uint16_t                    total_length;
    uint8_t                     num_esc_bytes;
    } IOP_msg_type;

#pragma pack(1)
typedef struct
    {
	uint16_t                    buf_idx;
	uint16_t					num_esc_bytes;
	uint16_t					payload_length;
    uint16_t                    chksm;
    } iop_tx_ctrl_type;
#pragma pack()

#ifdef IOP_RAM_LITE
	typedef struct
		{
		bool					inited;
		IOP_rx_state_type       rx_state;
		uint8_t *				tx_buf;
		uint32_t				tx_buf_length;
		uint8_t *				rx_buf;
		uint32_t				rx_buf_length;
		IOP_tx_func_type		tx_func;
		iop_tx_ctrl_type		tx_ctrl;
		void_func_type          reset_func;
		IOP_msg_type *     		iop_rx_msg;
		IOP_rx_msg_func_type 	rx_msg_hndlr;
		} IOP_ctrl_type;
#else
	typedef struct
		{
		bool					inited;
		IOP_rx_state_type       rx_state;
		UTL_circ_buf_t *		tx_cb;
		uint8_t *				tx_buf;
		UTL_circ_buf_t *		rx_cb;
		uint8_t *				rx_buf;
		IOP_tx_func_type		tx_func;
		void_func_type          reset_func;
		IOP_msg_type *     		iop_rx_msg;
		IOP_rx_msg_func_type 	rx_msg_hndlr;
		} IOP_ctrl_type;
#endif

//IOP Instrument IDs
typedef uint16_t iop_inst_id_type;

#include "IOP_inst_id.h"
#ifdef IOP_USE_PRJ
    #include "IOP_inst_id_prj.h"
#endif
#include "IOP_cmnd_id.h"

// iop_parser.c
void IOP_init
    ( void );

#ifdef IOP_RAM_LITE
	bool IOP_parse_rx_data
		(
		IOP_ctrl_type *			ctrl,
		uint8_t const *         data,
		uint32_t                size
		);
#else
	bool IOP_parse_rx_data
		(
		IOP_ctrl_type *				ctrl,
		const uint32_t        		num_bytes
		);
#endif

void IOP_pwrp
    ( void );

void IOP_register
	(
	IOP_ctrl_type *			ctrl,
	uint32_t				tx_buf_size,
	uint32_t				rx_buf_size
	);

void IOP_tx_msg
    (
    IOP_ctrl_type *				ctrl,
    iop_inst_id_type    		id,
    iop_msg_descriptor_type		dscptr,
    uint8_t const *             data,
    uint32_t                    size
    );


// iop_msg_handler.c
void IOP_msg_hndlr_init
	( void );

void IOP_rgstr_msg_hnldr
	(
	IOP_ctrl_type *			ctrl,
	IOP_rx_msg_func_type 	msg_hndlr
	);

void IOP_rx_msg_hndlr
	(
	IOP_ctrl_type *			ctrl,
	uint16_t                id,
	uint8_t const *         data,
	uint32_t                size
	);

void IOP_msg_hndlr_pwrp
	( void );

#endif /* IOP_H_ */
