/*
 * UTL_chksm.h
 *
 *  Created by Peter Carlson on Feb 4, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 *
 */

#ifndef UTL_CHKSM_H_
#define UTL_CHKSM_H_

uint8_t UTL_calc_chksum8
    (
    uint8_t   const * const data,
    uint32_t          const size
    );

uint16_t UTL_calc_chksum16
    (
    uint16_t  const * const data,
    uint32_t          const size
    );

uint32_t UTL_calc_chksum32
    (
    uint32_t  const * const data,
    uint32_t          const size
    );

#endif /* UTL_CHKSM_H_ */
