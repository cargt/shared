/*
 * utl_circ_buf.c
 *
 *  Created by Peter Carlson on Oct 2, 2013
 *  Copyright (c) 2019 Cargt, LLC. All rights reserved.
 */

#include <string.h>


#include "CARGT.h"
#include "UTL_circ_buf.h"


#if defined( BARE_METAL_IMPLEMENTATION ) && defined( __unix__ )
    #error "Can only define either BARE_METAL_IMPLEMENTATION or __unix__"
#endif

#if defined( __ICCARM__ )
	#include "cmsis_os.h"
#elif defined( __unix__ )
    #include <pthread.h>
#elif !defined( BARE_METAL_IMPLEMENTATION )
	#include "FreeRTOS.h"
	#include "task.h"
#endif


#if defined( BARE_METAL_IMPLEMENTATION )
/*
*   PROCEDURE NAME:
*       UTL_bare_metal_enter_critical - Bare metal critical execution
*
*   DESCRIPTION:
*       Override with a project-specific implementation to mask
*       applicable interrupts during critical periods of code execution.
*
*/
__attribute__((weak)) uint32_t UTL_bare_metal_enter_critical
    (
    void
    )
{
    return 0;
}

/*
*   PROCEDURE NAME:
*       UTL_bare_metal_exit_critical - Bare metal normal execution
*
*   DESCRIPTION:
*       Override with a project-specific implementation to unmask
*       applicable interrupts during critical periods of code execution.
*
*/
__attribute__((weak)) void UTL_bare_metal_exit_critical
    (
    uint32_t status
    )
{

}
#endif //defined( BARE_METAL_IMPLEMENTATION )

#if defined( __unix__ )
    pthread_mutex_t mutex_buffer_index = PTHREAD_MUTEX_INITIALIZER;
#endif

/*
*   PROCEDURE NAME:
*       UTL_circ_buf_free_space - Circular buffer free space
*
*   DESCRIPTION:
*       Returns the number of records of free space in the circular
*       buffer.
*
*/
uint32_t UTL_circ_buf_free_space
    (
    UTL_circ_buf_t * cb
    )
{
return( ( cb->data_size / cb->rcrd_size ) - UTL_circ_buf_used_space( cb ) );
} /* UTL_circ_buf_free_space() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_init - Initialize circular buffer
*
*   DESCRIPTION:
*       Initializes the circular buffer by allocating memory of size
*       bytes and setting indices to zero and indicating not full.
*
*/
void UTL_circ_buf_init
    (
    UTL_circ_buf_t * 	cb,
    uint8_t *				buf,
    uint32_t 				num_rcrds,
    uint32_t 				rcrd_size
    )
{
cb->mem = buf;
cb->data_size = num_rcrds * rcrd_size;
cb->rcrd_size = rcrd_size;
cb->read_idx = 0;
cb->write_idx = 0;
cb->full = false;
} /* UTL_circ_buf_init() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_read - Circular buffer read
*
*   DESCRIPTION:
*       Read up to (num_rcrds * rcrd_size) bytes from cb into buf. If buf is NULL,
*       the read_idx member of cb is updated but no memory is actually
*       copied. Returns the number of records read into buf.
*
*/
uint32_t UTL_circ_buf_read
    (
    UTL_circ_buf_t * 	cb,
    uint8_t * 			buf,
    uint32_t 				num_rcrds
    )
{
    return UTL_circ_buf_read_ex(cb, buf, num_rcrds, false);
}


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_read_ex - Circular buffer read
*
*   DESCRIPTION:
*       Read up to (num_rcrds * rcrd_size) bytes from cb into buf. If buf is NULL,
*       the read_idx member of cb is updated but no memory is actually
*       copied. Returns the number of records read into buf.
*       NOTE: peek_flag allows a read without updating the read_idx
*
*/
uint32_t UTL_circ_buf_read_ex
    (
    UTL_circ_buf_t * 	cb,
    uint8_t * 			buf,
    uint32_t 				num_rcrds,
    bool				peek_flag
    )
{
uint32_t              copy_size;
uint32_t              copy0;
uint32_t              copy1;
uint32_t              read_idx;
#if defined( BARE_METAL_IMPLEMENTATION )
    uint32_t          uxSavedInterruptStatus;
#elif defined( __unix__ )
    // no interrupt status available in user space
#else
    UBaseType_t       uxSavedInterruptStatus;
#endif

copy_size = UTL_circ_buf_used_space( cb );
copy_size = minval( copy_size, num_rcrds );

copy_size = copy_size * cb->rcrd_size;

if( !copy_size )
    {
    goto out_free;
    }

read_idx = cb->read_idx;


//If copy goes off the end of the buffer, do it in two parts.
//Otherwise do it in one big copy.
if( read_idx + copy_size > cb->data_size )
    {
    copy0 = cb->data_size - read_idx;
    copy1 = copy_size - copy0;
    if( buf )
        {
        memmove( buf, &cb->mem[read_idx], copy0 );
        buf += copy0;
        memmove( buf, cb->mem, copy1 );
        }
    read_idx = copy1;
    }
else
    {
    if( buf )
        {
        memmove( buf, &cb->mem[read_idx], copy_size );
        }
    read_idx += copy_size;
    if( read_idx == cb->data_size )
        {
        read_idx = 0;
        }
    }

//Update read index and full status. Disable interrupts
//if not running in interrupt context.
#if defined( BARE_METAL_IMPLEMENTATION )
    uxSavedInterruptStatus = UTL_bare_metal_enter_critical();
#elif defined( __unix__ )
    pthread_mutex_lock( &mutex_buffer_index );
#else
    uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR();
#endif
// only update if peek flag not set
if( !peek_flag )
    {
    cb->read_idx = read_idx;
    cb->full = false;
    }
#if defined( BARE_METAL_IMPLEMENTATION )
    UTL_bare_metal_exit_critical( uxSavedInterruptStatus );
#elif defined( __unix__ )
    pthread_mutex_unlock( &mutex_buffer_index );
#else
    taskEXIT_CRITICAL_FROM_ISR( uxSavedInterruptStatus );
#endif


out_free:
return( copy_size / cb->rcrd_size );
} /* UTL_circ_buf_read() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_reset_to - Reset read and write indexes
*
*   DESCRIPTION:
*       Reset the read and write pointers to a specific offset
*       in the buffer.
*
*/
void UTL_circ_buf_reset
    (
    UTL_circ_buf_t *     cb
    )
{
cb->full 		= false;
cb->write_idx	= 0;
cb->read_idx 	= 0;
} /* UTL_circ_buf_reset() */

/*
*   PROCEDURE NAME:
*       UTL_circ_buf_write - Circular buffer write
*
*   DESCRIPTION:
*       Write up to (num_rcrds * rcrd_size) bytes into cb from buf. If buf is NULL,
*       the write_idx member of cb is updated but no memory is actually
*       copied. Returns the number of records written into buf.
*
*/
uint32_t UTL_circ_buf_write
    (
    UTL_circ_buf_t *    cb,
    uint8_t const *     buf,
    uint32_t            num_rcrds
    )
{
uint32_t              copy_size;
uint32_t              copy0;
uint32_t              copy1;
uint32_t              write_idx;
#if defined( BARE_METAL_IMPLEMENTATION )
    uint32_t          uxSavedInterruptStatus;
#elif defined( __unix__ )
    // no interrupt status available in user space
#else
    UBaseType_t       uxSavedInterruptStatus;
#endif

copy_size = UTL_circ_buf_free_space( cb );
copy_size = minval( copy_size, num_rcrds );

copy_size = copy_size * cb->rcrd_size;

if( !copy_size )
    {
    goto out_free;
    }

write_idx = cb->write_idx;

//If copy goes off the end of the buffer, do it in two parts.
if( write_idx + copy_size > cb->data_size )
    {
    copy0 = cb->data_size - write_idx;
    copy1 = copy_size - copy0;
    if( buf != NULL )
        {
        memmove( &cb->mem[write_idx], buf, copy0 );
        buf += copy0;
        memmove( cb->mem, buf, copy1 );
        }
    write_idx = copy1;
    }
else
    {
    if( buf )
        {
        memmove( &cb->mem[write_idx], buf, copy_size );
        }
    write_idx += copy_size;
    if( write_idx == cb->data_size )
        {
        write_idx = 0;
        }
    }

//Update write index and full status.
#if defined( BARE_METAL_IMPLEMENTATION )
    uxSavedInterruptStatus = UTL_bare_metal_enter_critical();
#elif defined( __unix__ )
    pthread_mutex_lock( &mutex_buffer_index );
#else
    uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR();
#endif
cb->write_idx = write_idx;
cb->full = (cb->read_idx == cb->write_idx);
#if defined( BARE_METAL_IMPLEMENTATION )
    UTL_bare_metal_exit_critical( uxSavedInterruptStatus );
#elif defined( __unix__ )
    pthread_mutex_unlock( &mutex_buffer_index );
#else
    taskEXIT_CRITICAL_FROM_ISR( uxSavedInterruptStatus );
#endif

out_free:
return( copy_size / cb->rcrd_size );
} /* UTL_circ_buf_write() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_used_space - Circular buffer used space
*
*   DESCRIPTION:
*       Returns the number of records in cb.
*
*/
uint32_t UTL_circ_buf_used_space
    (
    UTL_circ_buf_t * cb
    )
{
if( true == cb->full )
    {
    return( cb->data_size / cb->rcrd_size );
    }
else if( cb->write_idx >= cb->read_idx )
    {
    return( ( cb->write_idx - cb->read_idx ) / cb->rcrd_size );
    }
else
    {
    return( ( cb->data_size - (cb->read_idx - cb->write_idx) ) / cb->rcrd_size );
    }
} /* UTL_circ_buf_used_space() */

