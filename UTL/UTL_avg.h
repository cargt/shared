/*
 * UTL_avg.h
 *
 *  Created by Bryce J TeBeest on May 11, 2017
 *  Copyright (c) 2017 Cargt, LLC. All rights reserved.
 *
 */

#ifndef UTL_AVG_H_
#define UTL_AVG_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct
    {
    uint16_t * avg_array;
    uint16_t   avg_index;
    uint16_t   avg_count;
    uint8_t    avg_count_shift;
    uint32_t   avg_total;
    uint16_t   current_avg;
    bool       stable;
    } utl_avg_uint16_t;

uint16_t UTL_avg_uint16( utl_avg_uint16_t * avg, uint16_t new );
bool UTL_avg_uint16_init( utl_avg_uint16_t * data, uint16_t * array, uint32_t count );
uint8_t UTL_bit_count( uint32_t input );
int8_t UTL_bit_pos( uint32_t input );

#endif /* UTL_AVG_H_ */
