/*
 * utl_chksm.c
 *
 *  Created by Peter Carlson on Oct 2, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 */

#include "CARGT.h"


/*
*   PROCEDURE NAME:
*       UTL_calc_chksum8 - Calculate 8-bit checksum
*
*   DESCRIPTION:
*
*/
uint8_t UTL_calc_chksum8
    (
    uint8_t   const * const data,
    uint32_t          const size
    )
{
//Local Variables
uint8_t			chksum 	= 0;
uint8_t const *	ptr 	= data;
uint32_t			i 		= 0;

while( i < size )
	{
	chksum += *ptr;
	ptr++;
	i++;
	}

return chksum;
}   /* UTL_calc_chksum8 */


/*
*   PROCEDURE NAME:
*       UTL_calc_chksum16 - Calculate 16-bit checksum
*
*   DESCRIPTION:
*
*/
uint16_t UTL_calc_chksum16
    (
    uint16_t  const * const data,
    uint32_t          const size
    )
{
//Local Variables
uint16_t			chksum 	= 0;
uint16_t const *	ptr 	= data;
uint32_t			i 		= 0;

while( i < ( size / sizeof( ptr ) ) )
	{
	chksum += *ptr;
	ptr++;
	i++;
	}

return chksum;
}   /* UTL_calc_chksum16 */


/*
*   PROCEDURE NAME:
*       UTL_calc_chksum32 - Calculate 32-bit checksum
*
*   DESCRIPTION:
*
*/
uint32_t UTL_calc_chksum32
    (
    uint32_t  const * const data,
    uint32_t          const size
    )
{
//Local Variables
uint32_t			chksum 	= 0;
uint32_t const *	ptr 	= data;
uint32_t			i 		= 0;

while( i < ( size / sizeof( ptr ) ) )
	{
	chksum += *ptr;
	ptr++;
	i++;
	}

return chksum;
}   /* UTL_calc_chksum32 */
