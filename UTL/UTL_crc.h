/*
 * UTL_crc.h
 *
 *  Created by Peter Carlson on Feb 4, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 *
 */

#ifndef UTL_CRC_H_
#define UTL_CRC_H_

#include "CARGT.h"

uint16_t UTL_calc_crc16
    (
    uint8_t   const * const data,
    uint32_t          const size,
    uint16_t          const initial_value
    );

uint32_t UTL_calc_crc32
    (
    uint8_t   const * const data,
    uint32_t          const size,
    uint32_t          const initial_value
    );

#endif /* UTL_CRC_H_ */
