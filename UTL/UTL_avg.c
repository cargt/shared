/*
 * utl_avg.c
 *
 *  Created by Bryce J TeBeest on May 11, 2017
 *  Copyright (c) 2017 Cargt, LLC. All rights reserved.
 *
 */

#include <string.h>

#include "UTL_avg.h"

/*
*   PROCEDURE NAME:
*       UTL_avg_uint16
*
*   DESCRIPTION:
*       Performs an average of an existing array of data and
*       returns (and stores) the new average value of the entire array.
*
*/
uint16_t UTL_avg_uint16( utl_avg_uint16_t * avg, uint16_t new )
{
avg->avg_total -= avg->avg_array[avg->avg_index];
avg->avg_total += new;
avg->avg_array[avg->avg_index] = new;
avg->avg_index++;
if( avg->avg_index >= avg->avg_count )
    {
    avg->avg_index = 0;
    avg->stable = true;
    }
avg->current_avg = ( avg->avg_total >> avg->avg_count_shift );
return avg->current_avg;
} //UTL_avg_uint16()

/*
*   PROCEDURE NAME:
*       UTL_avg_uint16_init
*
*   DESCRIPTION:
*       Initializes uint16 averaging structure.  This function currently
*       only supports powers of 2 for count elements to prevent a divide
*       in the actual averaging function later.
*
*/
bool UTL_avg_uint16_init( utl_avg_uint16_t * data, uint16_t * array, uint32_t count )
{
uint32_t i;
int8_t bit_pos;

bit_pos = UTL_bit_pos( count );
if( UTL_bit_pos < 0 )
    {
    return false;
    }

memset( array, 0, sizeof(uint16_t) * count );
data->avg_array = array;
data->avg_count = count;
data->avg_count_shift = bit_pos;
data->avg_index = 0;
data->avg_total = 0;
data->current_avg = 0;
data->stable = false;

return true;
} //UTL_avg_uint16_init()

/*
*   PROCEDURE NAME:
*       UTL_bit_count
*
*   DESCRIPTION:
*       Count the number of set bits in a given input variable.
*
*/
uint8_t UTL_bit_count( uint32_t input )
{
uint32_t bit_count = 0;
uint32_t bit_mask = 1;
uint32_t i;

for( i = 0; i <= sizeof( input ) * 8; i++ )
    {
    bit_count += ( ( bit_mask & input ) ? 1 : 0 );
    bit_mask <<= 1;
    }

return bit_count;
} //UTL_bit_count()

/*
*   PROCEDURE NAME:
*       UTL_bit_pos
*
*   DESCRIPTION:
*       Returns position (shift) of a bit in an input variable.
*       Returns negative number if zero or multiple bits are set.
*
*/
int8_t UTL_bit_pos( uint32_t input )
{
uint32_t i;

if( 1 != UTL_bit_count( input ) )
    {
    return -1;
    }

for( i = 0; i <= sizeof( input ) * 8; i++ )
    {
    if( ( 1 << i ) & input )
        {
        return i;
        }
    }

return -2;
} //UTL_bit_pos()

//------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------
