/*
 * UTL_circ_buf.h
 *
 *  Created by Peter Carlson on Feb 4, 2013
 *  Copyright (c) 2019 Cargt, LLC. All rights reserved.
 *
 */

#ifndef UTL_CIRC_BUF_H_
#define UTL_CIRC_BUF_H_

#include "CARGT.h"

typedef struct
    {
    uint32_t              rcrd_size;
    uint32_t              data_size;
    uint32_t              read_idx;
    uint32_t              write_idx;
    uint8_t *             mem;
    bool               	  full;
    } UTL_circ_buf_t;

uint32_t UTL_circ_buf_free_space
    (
    UTL_circ_buf_t * cb
    );

// Use sizeof( <object> ) you want to store as the record size.
// Examples:
//     uint8_t: num_rcrds is the number of bytes to store and rcrd_size = 1
//         Requires a buffer space of ( num_rcrds * 1 )
//     object of size 7: num_rcrds is the number of objects to store, and rcrd_size = 7
//         Requires a buffer space of ( num_rcrds * 7 )
void UTL_circ_buf_init
    (
    UTL_circ_buf_t * 	cb,
    uint8_t *			buf,
    uint32_t 			num_rcrds,
    uint32_t 			rcrd_size
    );

uint32_t UTL_circ_buf_read
    (
    UTL_circ_buf_t * 	cb,
    uint8_t * 			buf,
    uint32_t 			num_rcrds
    );

uint32_t UTL_circ_buf_read_ex
    (
    UTL_circ_buf_t * 	cb,
    uint8_t * 			buf,
    uint32_t 			num_rcrds,
    bool				peek_flag
    );

void UTL_circ_buf_reset
    (
    UTL_circ_buf_t *     cb
    );

uint32_t UTL_circ_buf_write
    (
    UTL_circ_buf_t *    cb,
    uint8_t const *     buf,
    uint32_t            num_rcrds
    );

uint32_t UTL_circ_buf_used_space
    (
    UTL_circ_buf_t * cb
    );

#endif /* UTL_CIRC_BUF_H_ */
